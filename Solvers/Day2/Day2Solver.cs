using System;
using System.Text.RegularExpressions;

namespace app {
    public class Day2Solver {
        public static int SolvePartOne() {
            // read in all input
            // get constraint
            // get letter
            // get password

            var allLines = GetInput();
            GetDay2PasswordLineFromText(allLines[0]);
            return 0;
        }

        private static string[] GetInput() {
            var allLines = System.IO.File.ReadAllLines(
                "./Solvers/Day2/AdventOfCode2020Day2Input.txt"
            );
            return allLines;
        }

        private static void GetDay2PasswordLineFromText(
            string text
        ) {
            var regexPattern = @"(<lowerBound>[0-9]+)-(<higherBound>[0-9]+)\s(<letterConstraint>[a-zA-Z]):\s(<password>[a-zA-Z]+)";
            var match = Regex.Match(text, regexPattern);
            Console.WriteLine(match.Groups["lowerBound"]);
            Console.WriteLine(match.Groups["higherBound"]);

            // return new Day2PasswordLine {

            // }
        }
    }

    public class Day2PasswordLine {
        public (int, int) passwordContraintBounds { get; set; }
        public string passwordConstraintLetter { get; set; }
        public string password { get; set; }
    }
}