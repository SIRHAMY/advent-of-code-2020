using System;
using System.Collections.Generic;
using System.Linq;

namespace app {
    public class Day1Solver {
        public static string SolvePartOne() {
            
            // Console.WriteLine(allLines.Length);

            // (int leftValue, int rightValue) = FindElementsThatSumToTarget(
            //     allLines,
            //     2020
            // ); 
            var haystackIntegers = GetHaystackIntegers();
            
            (int leftValue, int rightValue) = TwoSum(
                haystackIntegers,
                2020
            );

            return (leftValue * rightValue).ToString();
        }

        /*
            Approach - basically threesum
            * create a map of all nums in list and their counts
            * brute force list with two pointers
                * see if the 'missing' number is in map
        */
        public static string SolvePartTwo() {
            var haystackIntegers = GetHaystackIntegers();

            var values = ThreeSum(
                haystackIntegers,
                2020);
            return (values.Item1 * values.Item2 * values.Item3).ToString();
        }

        private static (int, int, int) ThreeSum(
            List<int> haystackIntegers,
            int target
        ) {
            var returnTuple = (-1, -1, -1);

            for(var i = 0; i < haystackIntegers.Count(); i++) {
                var currentValue = haystackIntegers[i];
                var currentTarget = target - currentValue;

                var listToSearch = haystackIntegers.GetRange(0, i);
                listToSearch.AddRange(haystackIntegers.GetRange(i, haystackIntegers.Count() - 1 - i));
                
                var values = TwoSum(
                    listToSearch,
                    currentTarget);

                if(values.Item1 != -1 && values.Item2 != -1) {
                    returnTuple = (currentValue, values.Item1, values.Item2);
                    break;
                }
            }
            return returnTuple;
        }

        private static List<int> GetHaystackIntegers() {
            var allLines = System.IO.File.ReadAllLines(
                "./Solvers/Day1/AdventOfCode2020Day1Input.txt"
            );
            List<int> haystackIntegers = allLines
                .Select(x => int.Parse(x))
                .ToList();
            return haystackIntegers;
        }

        private static (int, int) TwoSum(
            List<int> haystack,
            int target
        ) {
            var valueCounts = new Dictionary<int, int>();
            haystack.ForEach(
                x => {
                    if(valueCounts.ContainsKey(x)) {
                        valueCounts[x] = valueCounts[x] + 1;
                    } else {
                        valueCounts.Add(x, 1);
                    }
                }
            );

            var returnTuple = (-1, -1);
            haystack.ForEach(
                x => {
                    var numberNeeded = target - x;
                    if(valueCounts.ContainsKey(numberNeeded)) {
                        if(x == numberNeeded && valueCounts[x] == 1) {
                            // intentional no-op
                        } else {
                            returnTuple = (x, numberNeeded);
                        }
                    }
                }
            );
            return returnTuple;
        }

        // Solves in O(nlogn)
        // hamytodo - change to hash-based to go faster - O(n)
        private static (int, int) FindElementsThatSumToTarget(
            string[] haystack,
            int target
        ) {
            List<int> haystackIntegers = haystack
                .Select(x => int.Parse(x))
                .ToList();
            var sortedHaystackIntegers = new List<int>(haystackIntegers);
            sortedHaystackIntegers.Sort();
            return FindElementsThatSumToTargetInternal(
                sortedHaystackIntegers,
                target,
                0,
                sortedHaystackIntegers.Count() - 1
            );
        }

        private static (int, int) FindElementsThatSumToTargetInternal(
            List<int> sortedHaystackIntegers,
            int target,
            int leftBound,
            int rightBound
        ) {
            if(leftBound >= rightBound) {
                return (-1, -1);
            }

            int leftValue = sortedHaystackIntegers[leftBound];
            int rightValue = sortedHaystackIntegers[rightBound];

            var currentSum = leftValue + rightValue;
            if(currentSum == target) {
                return (leftValue, rightValue);
            } else if(currentSum < target) {
                return FindElementsThatSumToTargetInternal(
                    sortedHaystackIntegers,
                    target,
                    leftBound + 1,
                    rightBound
                );
            } else {
                return FindElementsThatSumToTargetInternal(
                    sortedHaystackIntegers,
                    target,
                    leftBound,
                    rightBound - 1
                );
            }
        }
    }
}