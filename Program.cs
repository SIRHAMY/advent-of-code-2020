﻿using System;
using app;

namespace app
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World2!");

            Console.WriteLine("Day1, Part1: " + Day1Solver.SolvePartOne());
            Console.WriteLine("Day1, Part2: " + Day1Solver.SolvePartTwo());
            Console.WriteLine("Day2, Part1: " + Day2Solver.SolvePartOne());

        }
    }
}
